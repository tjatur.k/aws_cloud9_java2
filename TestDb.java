import java.sql.*;
import java.util.logging.*;

public class TestDb {
    public static void main(String[] args) {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        
        String url = "jdbc:mysql://localhost:3306/c9?autoReconnect=true&useSSL=false";
        String user = "tjatur";
        String pass = "Tj12345*";
        
        try {
            con = DriverManager.getConnection(url, user, pass);
            st = con.createStatement();
            rs = st.executeQuery("SELECT * FROM Quotes");
            
            while (rs.next()) {
                System.out.println(rs.getString(2) + " -- " + rs.getString(3));
            }
        } catch (SQLException ex) {
            Logger lg = Logger.getLogger(TestDb.class.getName());
            lg.log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
                //System.out.println("In finally/try..., con:" + con.toString());
            } catch (SQLException ex) {
                Logger lg = Logger.getLogger(TestDb.class.getName());
                lg.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        // if (con != null) {
        //     System.out.println("Connection established!");
        // } else {
        //     System.out.println("Connection failed!...");
        // }
    }
}
