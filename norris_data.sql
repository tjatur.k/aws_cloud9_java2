drop table if exist Quotes, Authors;

create table if not exists Authors(Id int primary key auto_increment, Name varchar(25)) engine=InnoDB;
create table if not exists Quotes(Id int primary key auto_increment, AuthorId int, Quote varchar(100),
foreign key(AuthorId) references Authors(Id) on delete cascade) engine=InnoDB;

insert into Authors(Id, Name) values(1, 'Bruce Lee');
insert into Authors(Id, Name) values(2, 'Mr Miyagi');
insert into Authors(Id, Name) values(3, 'Rocky Balboa');
insert into Authors(Id, Name) values(4, 'Christie Brinkley');
insert into Authors(Id, Name) values(5, 'Peter Griffen');

insert into Quotes(Id, AuthorId, Quote) values(1, 1, 'Quote 1');
insert into Quotes(Id, AuthorId, Quote) values(2, 1, 'Quote 2');
insert into Quotes(Id, AuthorId, Quote) values(3, 2, 'Quote 3');
insert into Quotes(Id, AuthorId, Quote) values(4, 2, 'Quote 4');
insert into Quotes(Id, AuthorId, Quote) values(5, 3, 'Quote 5');
insert into Quotes(Id, AuthorId, Quote) values(6, 4, 'Quote 6');
insert into Quotes(Id, AuthorId, Quote) values(7, 4, 'Quote 7');
insert into Quotes(Id, AuthorId, Quote) values(8, 5, 'Quote 8');
insert into Quotes(Id, AuthorId, Quote) values(9, 5, 'Quote 9');
