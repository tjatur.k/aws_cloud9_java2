#!/bin/bash

cwd=$(pwd)
mkdir $cwd/tomcat
wget http://archive.apache.org/dist/tomcat/tomcat-9/v9.0.43/bin/apache-tomcat-9.0.43.tar.gz -O tomcat.tar.gz
tar xvf tomcat.tar.gz -C $cwd/tomcat --strip-components=1
